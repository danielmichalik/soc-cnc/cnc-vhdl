library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity stepClockSim is
end;

architecture bench of stepClockSim is

  component stepClock
      Port ( clk : in STD_LOGIC;
             period : in STD_LOGIC_VECTOR(31 downto 0);
             clkEnable : out STD_LOGIC);
  end component;

  signal clk: STD_LOGIC;
  signal period: STD_LOGIC_VECTOR(31 downto 0) := X"0000000A";
  signal clkEnable: STD_LOGIC;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: stepClock port map ( clk       => clk,
                            period    => period,
                            clkEnable => clkEnable );

  stimulus: process
  begin
    
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;