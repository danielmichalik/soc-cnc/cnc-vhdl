library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity motor is
    Port ( clk : in STD_LOGIC;
           requiredPosition : in STD_LOGIC_VECTOR (31 downto 0);
           actualPosition : out STD_LOGIC_VECTOR (31 downto 0);
           speed : in STD_LOGIC_VECTOR (31 downto 0);
           acceleration : in STD_LOGIC_VECTOR (31 downto 0);
           mode : in STD_LOGIC_VECTOR (1 downto 0);
           commands : in STD_LOGIC_VECTOR (31 downto 0);
           syncIn : in STD_LOGIC;
           syncOut : out STD_LOGIC;
           enable : out STD_LOGIC;
           dir : out STD_LOGIC;
           step : out STD_LOGIC;
           endSw : in STD_LOGIC);
end motor;

architecture Behavioral of motor is

signal syncSig, syncSW, dirSpd, resetCounter, pulseEnable, pulseClkEnable, endSwSig, endSwSW, pulseCountingEnable, dirSig : std_logic := '0';
signal counterValue : std_logic_vector(31 downto 0);

component motorControlFsm is
    Port ( clk : in STD_LOGIC;
        sync : in STD_LOGIC;
        mode : in STD_LOGIC_VECTOR (1 downto 0);
        endSw : in STD_LOGIC;
        dirSpd : in STD_LOGIC;
        goal : in STD_LOGIC_VECTOR (31 downto 0);
        counterValue : in STD_LOGIC_VECTOR (31 downto 0);
        resetCounter : out STD_LOGIC;
        dir : out STD_LOGIC;
        enable : out STD_LOGIC);
end component;

component pulseGenerator is
    Port ( clk : in STD_LOGIC;
           pulseEnable : in STD_LOGIC;
           pulseOutput : out STD_LOGIC);
end component;

component stepClock is
    Port ( clk : in STD_LOGIC;
           period : in STD_LOGIC_VECTOR(31 downto 0);
           clkEnable : out STD_LOGIC);
end component;

component stepCounter is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           clkCounter : in STD_LOGIC;
           enable : in STD_LOGIC;
           dir : in STD_LOGIC;
           count : out STD_LOGIC_VECTOR (31 downto 0);
           pulse : out STD_LOGIC);
end component;

begin

    motorControlFsm0:motorControlFsm
    port map (
        clk => clk,
        sync => syncSig,
        mode => mode,
        endSw => endSwSig,
        dirSpd => dirSpd,
        goal => requiredPosition,
        counterValue => counterValue,
        resetCounter => resetCounter,
        dir => dirSig,
        enable => pulseCountingEnable
    );
    
    pulseGenerator0:pulseGenerator
    port map(
        clk => clk,
        pulseEnable => pulseEnable,
        pulseOutput => step
    );
    
    stepClock0:stepClock
    port map (
        clk => clk,
        period => speed,
        clkEnable => pulseClkEnable
    );
    
    stepCounter0:stepCounter
    port map (
        clk => clk,
        reset => endSwSig,
        clkCounter => pulseClkEnable,
        enable => pulseCountingEnable,
        dir => dirSig,
        count => counterValue,
        pulse => pulseEnable
    );
    
    -- fuc**** wiring
    syncSig <= syncIn or syncSW;
    syncOut <= syncSig;
    endSwSig <= endSw or endSwSW;
    dir <= dirSig;
    actualPosition <= counterValue;
    -- CMDs
    enable <= not(commands(0));
    syncSW <= commands(1);
    endSwSW <= commands(2);

end Behavioral;
