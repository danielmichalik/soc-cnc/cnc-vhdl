library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity motor_tb is
end;

architecture bench of motor_tb is

  component motor
      Port ( clk : in STD_LOGIC;
             requiredPosition : in STD_LOGIC_VECTOR (31 downto 0);
             actualPosition : out STD_LOGIC_VECTOR (31 downto 0);
             speed : in STD_LOGIC_VECTOR (31 downto 0);
             acceleration : in STD_LOGIC_VECTOR (31 downto 0);
             mode : in STD_LOGIC_VECTOR (1 downto 0);
             commands : in STD_LOGIC_VECTOR (31 downto 0);
             syncIn : in STD_LOGIC;
             syncOut : out STD_LOGIC;
             enable : out STD_LOGIC;
             dir : out STD_LOGIC;
             step : out STD_LOGIC;
             endSw : in STD_LOGIC);
  end component;

  signal clk: STD_LOGIC;
  signal requiredPosition: STD_LOGIC_VECTOR (31 downto 0) := X"80000010";
  signal actualPosition: STD_LOGIC_VECTOR (31 downto 0);
  signal speed: STD_LOGIC_VECTOR (31 downto 0):= X"00008000";
  signal acceleration: STD_LOGIC_VECTOR (31 downto 0):=(others=>'0');
  signal mode: STD_LOGIC_VECTOR (1 downto 0):="00";
  signal commands: STD_LOGIC_VECTOR (31 downto 0):=(others=>'0');
  signal syncIn: STD_LOGIC := '0';
  signal syncOut: STD_LOGIC;
  signal enable: STD_LOGIC;
  signal dir: STD_LOGIC;
  signal step: STD_LOGIC;
  signal endSw: STD_LOGIC:='0';
  
  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean := false;

begin

  uut: motor port map ( clk              => clk,
                        requiredPosition => requiredPosition,
                        actualPosition   => actualPosition,
                        speed            => speed,
                        acceleration     => acceleration,
                        mode             => mode,
                        commands         => commands,
                        syncIn           => syncIn,
                        syncOut          => syncOut,
                        enable           => enable,
                        dir              => dir,
                        step             => step,
                        endSw            => endSw );

  stimulus: process
  begin
    wait for clock_period;
    syncIn <= '1';
    wait for clock_period;
    syncIn <= '0';
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;
end;