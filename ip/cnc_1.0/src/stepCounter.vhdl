library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity stepCounter is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           clkCounter : in STD_LOGIC;
           enable : in STD_LOGIC;
           dir : in STD_LOGIC;
           count : out STD_LOGIC_VECTOR (31 downto 0);
           pulse : out STD_LOGIC);
end stepCounter;

architecture Behavioral of stepCounter is

signal counterSig:std_logic_vector(31 downto 0):=X"80000000";
signal pulseSig:std_logic;

begin

process(clk)
begin
    if (clk'event and clk = '1') then
        if reset = '1' then
            counterSig <= X"80000000";
            pulseSig <= '0';
        elsif (clkCounter = '1' and enable = '1') then
            if dir = '1' then
                counterSig <= counterSig + 1;
            else
                counterSig <= counterSig - 1;
            end if;
            pulseSig <= '1';
        else
            pulseSig <= '0';
        end if;
    end if;
end process;

count <= counterSig;
pulse <= pulseSig;

end Behavioral;
