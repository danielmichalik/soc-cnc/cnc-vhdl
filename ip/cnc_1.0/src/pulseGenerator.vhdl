library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pulseGenerator is
    Port ( clk : in STD_LOGIC;
           pulseEnable : in STD_LOGIC;
           pulseOutput : out STD_LOGIC);
end pulseGenerator;

architecture Behavioral of pulseGenerator is

signal pulseActive:std_logic:='0';
signal counter:std_logic_vector(23 downto 0):=(others=>'0'); -- TODO: optimize

begin

process(clk)
begin
    if (clk'event and clk = '1') then
        if (pulseEnable = '1' and pulseActive = '0') then -- start of pulse
            pulseActive <= '1';
            counter <= counter + 1;
        elsif (pulseActive = '1') then
            if (counter < 1000) then -- for 100MHz input => 10us
                counter <= counter + 1;
            else
                counter <= (others=>'0');
                pulseActive <= '0';
            end if;
        end if;
    end if;
end process;

pulseOutput <= pulseActive;

end Behavioral;
