library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity stepCounterSim is
end;

architecture bench of stepCounterSim is

  component stepCounter
      Port ( clk : in STD_LOGIC;
             reset : in STD_LOGIC;
             clkCounter : in STD_LOGIC;
             enable : in STD_LOGIC;
             dir : in STD_LOGIC;
             count : out STD_LOGIC_VECTOR (31 downto 0);
             pulse : out STD_LOGIC);
  end component;

  signal clk: STD_LOGIC;
  signal reset: STD_LOGIC := '0';
  signal clkCounter: STD_LOGIC := '0';
  signal enable: STD_LOGIC := '0';
  signal dir: STD_LOGIC := '0';
  signal count: STD_LOGIC_VECTOR (31 downto 0);
  signal pulse: STD_LOGIC;

  constant clock_period: time := 10 ns;
  constant sub_clock_period: time := 40 ns;
  signal stop_the_clock: boolean;

begin

  uut: stepCounter port map ( clk        => clk,
                              reset      => reset,
                              clkCounter => clkCounter,
                              enable     => enable,
                              dir        => dir,
                              count      => count,
                              pulse      => pulse );

  stimulus: process
  begin
    wait for sub_clock_period * 2;
    enable <= '1';
    wait for sub_clock_period * 3;
    enable <= '0';
    wait for sub_clock_period * 2;
    dir <= '1';
    enable <= '1';
    wait for sub_clock_period * 5;
    enable <= '0';
    wait for sub_clock_period * 2;
    reset <= '1';
    wait for clock_period;
    reset <= '0';
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '1', '0' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

  subClocking: process
  begin
    while not stop_the_clock loop
      clkCounter <= '1', '0' after clock_period / 2;
      wait for sub_clock_period;
    end loop;
    wait;
  end process;

end;