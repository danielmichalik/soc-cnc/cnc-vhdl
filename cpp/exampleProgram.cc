 #include "platform.h"
#include "uartReimp.h"
#include "xparameters.h"
#include "XMemoryAccess.hpp"
#include "XTimerControl.hpp"
#include "StepperMotor.hpp"
#include "MovementControl.hpp"
#include "ToolControl.hpp"
#include "Command.hpp"
#include "GCodeArbiter.hpp"
#include "Utils.hpp"
#include "Pid.hpp"
#include <memory>
#include "sleep.h"

void SendString(std::string &str);
void ProcessCommand(std::unique_ptr<Command> command);
void SendSimpleCommandResponse(CommandType type);
void BufferCyclicWork();

std::unique_ptr<StepperMotor> stepperMotorAxisX;
std::unique_ptr<StepperMotor> stepperMotorAxisY;
std::unique_ptr<StepperMotor> stepperMotorAxisZ;

std::unique_ptr<MovementControl> movementControl;
std::unique_ptr<XTimerControl> pwmTimerControl;
std::unique_ptr<ToolControl> toolControl;
std::unique_ptr<GCodeArbiter> gCodeArbiter;
std::unique_ptr<CommandBuffer> commandBuffer;

std::string rxBuffer("");

bool started = false;
bool doStep = false;

int main()
{
	init_platform();

	auto memoryAccessAxisX = std::make_unique<XMemoryAccess>(XPAR_CNCX_S00_AXI_BASEADDR, 40);
	auto memoryAccessAxisY = std::make_unique<XMemoryAccess>(XPAR_CNCY_S00_AXI_BASEADDR, 40);
	auto memoryAccessAxisZ = std::make_unique<XMemoryAccess>(XPAR_CNCZ_S00_AXI_BASEADDR, 40);

	stepperMotorAxisX = std::make_unique<StepperMotor>(std::move(memoryAccessAxisX));
	stepperMotorAxisY = std::make_unique<StepperMotor>(std::move(memoryAccessAxisY));
	stepperMotorAxisZ = std::make_unique<StepperMotor>(std::move(memoryAccessAxisZ));

	movementControl = std::make_unique<MovementControl>(
			std::move(stepperMotorAxisX),
			std::move(stepperMotorAxisY),
			std::move(stepperMotorAxisZ));

	pwmTimerControl = std::make_unique<XTimerControl>(
			XPAR_AXI_TIMER0_PWM_BASEADDR,
			XPAR_AXI_TIMER0_PWM_CLOCK_FREQ_HZ);
	pwmTimerControl->Init();
	pwmTimerControl->StartPwm(50, 5);

	auto spindlePid = std::make_unique<Pid>(2, 0, 0, 0.02, 0, 0xFFFF);
	toolControl = std::make_unique<ToolControl>(
			std::move(pwmTimerControl),
			std::move(spindlePid));

	gCodeArbiter = std::make_unique<GCodeArbiter>(
			std::move(movementControl),
			std::move(toolControl));

	while(1)
	{
		u8 ch;
		if (UartReimp_RecvByte(STDOUT_BASEADDRESS, &ch))
		{
			rxBuffer = rxBuffer + (char)ch;
		}

		if (rxBuffer.find("\u0003") != std::string::npos)
		{

			for(int i = 0; i < 1; i++)
			{
				try
				{
					xil_printf(rxBuffer.c_str());
					auto command = Command::Parse(rxBuffer);
					ProcessCommand(std::move(command));
					break;
				}
				catch(const std::bad_alloc& e)
				{
					xil_printf("ex%d", i);
				}
			}
			rxBuffer = std::string("");
		}
		if((started || doStep) && !commandBuffer->IsEmpty())
		{
			BufferCyclicWork();
		}
		doStep = false;
	}

	cleanup_platform();
	return 0;
}

void SendString(std::string &str)
{
	for(char& c : str)
	{
		UartReimp_SendByte(STDOUT_BASEADDRESS, (u8)c);
	}
}

void ProcessCommand(std::unique_ptr<Command> command)
{
	if (command->Type == CommandType::ECHO)
	{
		SendSimpleCommandResponse(CommandType::ECHO);
	}
	else if (command->Type == CommandType::GET_POS)
	{
		auto coords = GetActualCoords();
		std::vector<Argument> arguments;
		arguments.push_back(Argument(coords[0]));
		arguments.push_back(Argument(coords[1]));
		arguments.push_back(Argument(coords[2]));
		auto responseCommand = Command(CommandType::GET_POS, arguments);
		std::string response = responseCommand.GetString();
		SendString(response);
	}
	else if (command->Type == CommandType::G_CODE)
	{
		commandBuffer->Push(command);
	}
	else if (command->Type == CommandType::START)
	{
		started = true;
		SendSimpleCommandResponse(CommandType::START);
	}
	else if (command->Type == CommandType::STOP)
	{
		started = false;
		SendSimpleCommandResponse(CommandType::STOP);
	}
	else if (command->Type == CommandType::STEP)
	{
		doStep = true;
		SendSimpleCommandResponse(CommandType::STEP);
	}
	else if (command->Type == CommandType::RESET)
	{
		#warning "Not implemented yet"
		SendSimpleCommandResponse(CommandType::RESET);
	}
	else
	{
		auto responseCommand = Command(CommandType::ERR);
		std::string response = responseCommand.GetString();
		SendString(response);
	}
}

void SendSimpleCommandResponse(CommandType type)
{
	auto responseCommand = Command(type);
	std::string response = responseCommand.GetString();
	SendString(response);
}

void BufferCyclicWork()
{
	if(commandBuffer->IsEmpty())
	{
		return;
	}

	auto command = commandBuffer->Pop();
	if (gCodeArbiter->ProcessGcode(command) == ProcesResult::PASS)
	{
		SendSimpleCommandResponse(CommandType::G_CODE);
	}
	else
	{
		SendSimpleCommandResponse(CommandType::ERR);
	}
}