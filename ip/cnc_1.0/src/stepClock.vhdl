library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity stepClock is
    Port ( clk : in STD_LOGIC;
           period : in STD_LOGIC_VECTOR(31 downto 0);
           clkEnable : out STD_LOGIC);
end stepClock;

architecture Behavioral of stepClock is

signal counter:std_logic_vector(31 downto 0):=(others=>'0');
signal clkEnableSig:std_logic:='0';

begin

process(clk)
begin
    if (clk'event and clk='1') then
        if counter < (period - 1) then
            counter <= counter + 1;
            clkEnableSig <= '0';
        else
            counter <= (others=>'0');
            clkEnableSig <= '1';
        end if;
    end if;
end process;

clkEnable <= clkEnableSig;

end Behavioral;
