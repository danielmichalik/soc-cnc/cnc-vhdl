library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity motorControlFsm is
    Port ( clk : in STD_LOGIC;
        sync : in STD_LOGIC;
        mode : in STD_LOGIC_VECTOR (1 downto 0);
        endSw : in STD_LOGIC;
        dirSpd : in STD_LOGIC;
        goal : in STD_LOGIC_VECTOR (31 downto 0);
        counterValue : in STD_LOGIC_VECTOR (31 downto 0);
        resetCounter : out STD_LOGIC;
        dir : out STD_LOGIC;
        enable : out STD_LOGIC);
end motorControlFsm;

architecture Behavioral of motorControlFsm is

type state_type is (IDLE, RUNNING_POS, RUNNING_SPD, CALIB);
signal state, next_state : state_type := IDLE;
signal dirSig, enableSig : std_logic := '0';

begin

SYNC_PROC: process (clk)
begin
    if (clk'event and clk = '1') then
        state <= next_state;
        enable <= enableSig;
        dir <= dirSig;
    end if;
end process;

OUTPUT_DECODE: process (state, sync, counterValue, goal, mode, dirSpd)
begin
    if ((state = IDLE) and (sync = '1') and (goal /= counterValue)) then
        enableSig <= '1';
    elsif ((state = IDLE) and (sync = '1') and (mode = "01")) then
        enableSig <= '1';
    elsif ((state = RUNNING_POS) and (goal /= counterValue)) then
        enableSig <= '1';
    elsif ((state = RUNNING_SPD) and (sync = '1') and (mode = "01")) then
        enableSig <= '1';
    else
        enableSig <= '0';
    end if;

    if ((state = CALIB) and (endSw = '1')) then
        resetCounter <= '1';
    else
        resetCounter <= '0';
    end if;
    
    if ((state = RUNNING_SPD) or (state = CALIB)) then
        dirSig <= dirSpd;
    else 
        if (goal > counterValue) then
            dirSig <= '1';
        else
            dirSig <= '0';
        end if;
    end if;
end process;

NEXT_STATE_DECODE: process (state, counterValue, goal, sync, mode, endSw)
begin
    next_state <= state;
    case (state) is
        when RUNNING_POS =>
            if goal = counterValue then
                next_state <= IDLE;
            end if;
        when RUNNING_SPD =>
            if ((sync = '0') or (mode /= "01")) then
                next_state <= IDLE;
            end if;
        when CALIB =>
            if endSw = '1' then
                next_state <= IDLE;
            end if;
        when IDLE =>
            if ((sync = '1') and (goal /= counterValue)) then
                next_state <= RUNNING_POS;
            end if;
            if ((sync = '1') and (mode = "01")) then
                next_state <= RUNNING_SPD;
            end if;
            if ((sync = '1') and (mode = "10")) then
                next_state <= CALIB;
            end if;
        when others =>
           next_state <= IDLE;
    end case;
end process;

end Behavioral;
