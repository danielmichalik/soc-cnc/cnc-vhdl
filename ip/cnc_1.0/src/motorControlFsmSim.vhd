library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Std_logic_unsigned.all;
use IEEE.Numeric_Std.all;

entity motorControlFsmSim is
end;

architecture bench of motorControlFsmSim is

  component motorControlFsm
      Port ( clk : in STD_LOGIC;
          sync : in STD_LOGIC;
          mode : in STD_LOGIC_VECTOR (1 downto 0);
          endSw : in STD_LOGIC;
          dirSpd : in STD_LOGIC;
          goal : in STD_LOGIC_VECTOR (31 downto 0);
          counterValue : in STD_LOGIC_VECTOR (31 downto 0);
          resetCounter : out STD_LOGIC;
          dir : out STD_LOGIC;
          enable : out STD_LOGIC);
  end component;

  signal clk: STD_LOGIC;
  signal sync: STD_LOGIC := '0';
  signal mode: STD_LOGIC_VECTOR (1 downto 0) := "00";
  signal endSw: STD_LOGIC := '0';
  signal dirSpd: STD_LOGIC := '0';
  signal goal: STD_LOGIC_VECTOR (31 downto 0) := X"80000000";
  signal counterValue: STD_LOGIC_VECTOR (31 downto 0) := X"80000000";
  signal resetCounter: STD_LOGIC;
  signal dir: STD_LOGIC;
  signal enable: STD_LOGIC;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: motorControlFsm port map ( clk          => clk,
                                  sync         => sync,
                                  mode         => mode,
                                  endSw        => endSw,
                                  dirSpd       => dirSpd,
                                  goal         => goal,
                                  counterValue => counterValue,
                                  resetCounter => resetCounter,
                                  dir          => dir,
                                  enable       => enable );

  stimulus: process
  begin
    -- POS mode
    wait for clock_period * 2;
    goal <= goal + 3;
    sync <= '1';
    wait for clock_period;
    sync <= '0';
    
    -- counting
    wait for clock_period;
    counterValue <= counterValue + 1;
    wait for clock_period;
    counterValue <= counterValue + 1;
    wait for clock_period;
    counterValue <= counterValue + 1;
    
    -- SPD mode
    wait for clock_period * 2;
    mode <= "01";
    sync <= '1';
    wait for clock_period * 4;
    dirSpd <= '1';              -- change dir during move
    wait for clock_period * 4;
    sync <= '0';
    dirSpd <= '0';
    
    -- CALIB
    wait for clock_period * 4;
    mode <= "10";
    sync <= '1';
    wait for clock_period;
    sync <= '0';
    wait for clock_period * 3;
    dirSpd <= '1';              -- change dir during move, only logic check
    wait for clock_period * 3;
    endSw <= '1';
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;