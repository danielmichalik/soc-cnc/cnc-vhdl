library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity pulseGeneratorSim is
end;

architecture bench of pulseGeneratorSim is

  component pulseGenerator
      Port ( clk : in STD_LOGIC;
             pulseEnable : in STD_LOGIC;
             pulseOutput : out STD_LOGIC);
  end component;

  signal clk: STD_LOGIC;
  signal pulseEnable: STD_LOGIC := '0';
  signal pulseOutput: STD_LOGIC := '0';

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: pulseGenerator port map ( clk         => clk,
                                 pulseEnable => pulseEnable,
                                 pulseOutput => pulseOutput );

  stimulus: process
  begin
    wait for clock_period;
    pulseEnable <= '1';
    wait for clock_period;
    pulseEnable <= '0';
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;